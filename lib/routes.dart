import 'package:aniwatch/pages/home.dart';
import 'package:aniwatch/pages/watch.dart';

var appRoutes =  {
  "/": (context) => const Homepage(),
  "/watch": (content) => const WatchPage(),
};