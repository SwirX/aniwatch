[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/V7V4WPVEJ)

# AniWatch

AniWatch is a simple mobile application built with Flutter that allows users to watch anime episodes conveniently on their devices. With a user-friendly interface and a vast collection of anime titles, AniWatch provides an enjoyable anime viewing experience.

## Features

- **Browse Anime Titles**: Explore a wide range of anime titles conveniently categorized for easy navigation.
- **Watch Anime Episodes**: Stream anime episodes directly within the app using the built-in video player.
- **Bookmark Favorites**: Save favorite anime series or episodes for quick access later.
- **Search Functionality**: Easily search for specific anime titles or episodes using the search feature.

## Usage

1. **Clone the Repository**: Clone the AniWatch repository to your local machine.
   ```bash
   git clone https://github.com/SwirX/aniwatch.git
   ```

2. **Navigate to Project Directory**: Move into the AniWatch project directory.
   ```bash
   cd aniwatch
   ```

3. **Install Dependencies**: Ensure all required dependencies are installed using Flutter.
   ```bash
   flutter pub get
   ```

4. **Run the App**: Launch the AniWatch app on an emulator or physical device.
   ```bash
   flutter run
   ```

## Alternative: Precompiled Binaries

If you prefer not to compile the source code yourself, precompiled binaries (currently only android available) are available. Simply download the binary from the [Releases](https://github.com/SwirX/aniwatch/releases) and install it to start using AniWatch immediately.

## CLI Version

For users who prefer a command-line interface (CLI) experience, an alternative CLI program [AniWatch-CLI](https://github.com/SwirX/aniwatch-cli) is available. It provides similar functionality to AniWatch but allows users to watch anime episodes directly from the terminal.

## Contributing

Contributions to AniWatch are welcome! If you encounter any issues or have suggestions for improvements, feel free to open an issue or submit a pull request.

## License

This project is licensed under the GPL-3.0 - see the [LICENSE](LICENSE) file for details.

---